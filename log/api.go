package log

// a customized version of google.golang.org/appengine/log which the file and line number is prepended to each log

import (
	"runtime"

	"golang.org/x/net/context"
	log2 "google.golang.org/appengine/log"
)

// Debugf is a wrapper function with file name and line number added
func Debugf(ctx context.Context, format string, args ...interface{}) {
	log2.Debugf(ctx, "%s:%d "+format, fileline(args...)...)
}

// Infof is a wrapper function with file name and line number added
func Infof(ctx context.Context, format string, args ...interface{}) {
	log2.Infof(ctx, "%s:%d "+format, fileline(args...)...)
}

// Warningf is a wrapper function with file name and line number added
func Warningf(ctx context.Context, format string, args ...interface{}) {
	log2.Warningf(ctx, "%s:%d "+format, fileline(args...)...)
}

// Errorf is a wrapper function with file name and line number added
func Errorf(ctx context.Context, format string, args ...interface{}) {
	log2.Errorf(ctx, "%s:%d "+format, fileline(args...)...)
}

// Criticalf is a wrapper function with file name and line number added
func Criticalf(ctx context.Context, format string, args ...interface{}) {
	log2.Criticalf(ctx, "%s:%d "+format, fileline(args...)...)
}

func fileline(args ...interface{}) []interface{} {
	_, file, line, _ := runtime.Caller(2)
	args = append([]interface{}{line}, args...)
	args = append([]interface{}{file}, args...)
	return args
}
